# About

A sandbox repository to allow playing around with various Git workflows

## Scenarios To Play With

Based on [this workflow](http://nvie.com/posts/a-successful-git-branching-model/)

1. Plan a new release
2. Split release off into more than one feature
3. Hot fix required during release development
4. Merge feature branches into release
5. Merge and tag release
